<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Superfast Satellite Broadband - satguys</title>
	<link rel="stylesheet" href="dist/css/app.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style>

      body {

           font-family: Arial, Helvetica, sans-serif;
       }
       .button-wrapper {
  display: block;
  text-align: center;
}

.get-started {

    text-align: center;
}
.button {
  border: none;
  box-shadow: 2px 2px 2px rgba(0,0,0,0.2);
  font-size: 1em;
  width: 100%;
  background-color: blue;
  
}

.button2 {

    border: none;
  border-radius: 3em;
  box-shadow: 2px 2px 2px rgba(0,0,0,0.2);
  display: inline-block;
  font-size: 1em;
  padding: 1em 2em;
  width: auto;  
}
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 40%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.container {
  padding: 2px 16px;
}


.orange-container {

    background-color: orange;
    color: #fff;
    padding:5px;
    
}

.gold-container {

background-color: gold;
color: #fff;
padding:5px;
}

.silver-container {

background-color: silver;
color: #000;
padding:5px;
}

.card-container {

    display: flex;
    padding: 15px;
    margin: .25rem;
}
   </style>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<script>
    $( function() {
      $( "#accordion" ).accordion();
    } );
    </script>

</head>


<body>

	<div class="comp__contact-bar py-2 py-xl-0">
		<div class="container">
			<div class="row align-items-center d-xl-none">
				<div class="col-4">
					<a href="#" id="nav-toggle">
						<img
						class="icon open"
						src="dist/img/navigation-icon.svg"
						alt="Navigation Icon Open"
						>

						<img
						class="icon close" 
						src="dist/img/times-white-icon.svg"
						alt="Navigation Icon Close"
						>
					</a>
				</div>

				<div class="col-8 text-right">
					<img
					class="icon smaller mr-1"
					src="dist/img/phone-icon.svg"
					alt="Contact Us"
					>

					<a class="form-number" href="tel:+441691662712">01691 662 712</a>
				</div>
			</div>

			<nav class="comp__navigation">
				<div class="d-none d-xl-inline-block">
					<img
					src="dist/img/satguys_logo--white.svg"
					width="100"
					>
				</div>

				<div class="d-xl-flex flex-xl-column">
					<ul class="order-xl-2">
						<li>
							<a href="#three-easy-steps">3 easy steps</a>
						</li>

						<li>
							<a href="#why-choose-us">Why choose us?</a>
						</li>

	

						<li>
							<a href="#trusted-by">Trusted by</a>
						</li>

						<li>
							<a href="#faqs">FAQs</a>
						</li>

						<li>
							<a href="#packages">Packages</a>
						</li>

	<div class="button-wrapper">
        <a class="button2 cta-button">Free WIFI - Over 65's get FREE wifi</a>
      </div>

      <div class="button-wrapper">
        <a class="button2 cta-button">Cheapest in the country - We will not be beaten on price</a>
      </div>


		

						<li>
							<a href="#" target="_blank">Existing Customers</a>
						</li>
					</ul>

					<div class="py-2 text-center text-xl-right order-xl-1 pr-xl-3">
						<strong class="d-inline-block mr-2">Call our sales team </strong>

						<a class="menu-number" href="tel:+441691662712">01691 662 712</a>
					</div>
				</div>
			</nav>
		</div>
	</div>

	<header class="core__header">
		<div class="container">
			<img
			class="logo d-xl-none"
			src="dist/img/satguys_logo--white.svg"
			alt="satguys logo"
			>

			<h1>
				Fast, Reliable <span class="satellite">Satellite</span> <span class="broadband">Broadband</span>
			</h1>
		</div>

		<div class="tagline">
			<div class="container shrink">
				<div class="row align-items-center">
					<div class="col-4 col-lg-2 text-sm-right">
						<div class="coverage-animation-wrapper mr-sm-4">
							<img
							class="d-block"
							src="dist/img/coverage-graphic-uk.svg"
							alt="100% Nationwide Coverage"
							width="40"
							>

							<div class="coverage-animation">
								<div class="pulses-wrapper">
									<span class="pulseThree"></span>
									<span class="pulseTwo"></span>
									<span class="pulseOne"></span>
								</div>
							</div>

							<img
							class="satellite-icon"
							src="dist/img/coverage-graphic-satellite.svg"
							width="30"
							>
						</div>
					</div>

					<div class="col-8 col-lg-10 text-left text-lg-center">
						Covering 100% of the UK even in rural locations.
					</div>
				</div>
			</div>
		</div>

		<a
		href="#"
		class="btn btn-orange btn-lg rounded-pill join-today mb-5"
		>
			Get Started
		</a>
	</header>

	<main class="core__body">
		<div class="comp__block white p-0">
			<div class="container">
				<div class="intro-text">
					<p>
						With 115,000 customers across 30 countries, satguys is the world’s largest multi territory alternative broadband service provider connecting consumers, businesses and governments to broadband via satellite.
					</p>

					<div class="row align-items-center">
						<div class="col-12 col-xl-4">
							<p class="price">
								From &pound;24.99<small>/month</small>
							</p>
						</div>

						<div class="col-12 col-xl-8">
							<div class="comp__packages">
								<div class="package">
									<ul class="summary-list">
										<li>
											<a href="#" class="helper-info average-speed">
												<span>
													Average speeds of <strong>36Mbps</strong> available
												</span>

												<span class="helper">
													<img class="open" src="dist/img/question-icon.svg">
													<img class="close" src="dist/img/times-icon.svg">
												</span>
											</a>

											<span class="helper-text">
												<p>
													Mbps = Megabits per second.
												</p>

												<p>
													The speed will vary according to network traffic at any given time and typically during peak hours (5-11pm in the UK).
												</p>
											</span>
										</li>

										<li>
											<a href="#" class="helper-info data-usage">
												<strong>Unlimited Usage</strong>

												<span class="helper">
													<img class="open" src="dist/img/question-icon.svg">
													<img class="close" src="dist/img/times-icon.svg">
												</span>
											</a>

											<span class="helper-text">
												<p>
													Unlike other providers, we promise not to turn off your connection when you have reached your data allowance.
												</p>
											</span>
										</li>

										<li>
											<a href="#" class="helper-info uptime">
												<strong>99.5% Uptime</strong>

												<span class="helper">
													<img class="open" src="dist/img/question-icon.svg">
													<img class="close" src="dist/img/times-icon.svg">
												</span>
											</a>

											<span class="helper-text">
												<p>
													This is the overall performance of our satellite, which is better than many fixed-line services.
												</p>
											</span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="comp__block blue">
			<div class="container text-center text-xl-left">
				<div class="row align-items-xl-center">
					<div class="col-12 col-xl-8">
						<h2 class="mb-xl-0">Join 115,000 users already using satguys</h2>
					</div>

					<div class="col-12 col-xl-4 text-xl-right">
						<a
						href="#"
						target="_blank"
						class="btn btn-orange rounded-pill px-5 mb-xl-0 join-today-100k"
						>
							Get Started
						</a>
					</div>
				</div>

				<p class="mb-0 d-xl-none">
					14 day risk-free guarantee
				</p>
			</div>
		</section>

		<section class="comp__block comp__review has-media">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6 media-col order-lg-2">
						<img
						class="d-block mx-auto mb-3 mb-lg-0"
						data-src="dist/img/satguys-support-woman.jpg"
						alt="satguys support staff"
						>
					</div>

					<div class="col-12 col-lg-6 order-lg-1">
						<div class="overall-reviews">
							<div class="row align-items-center">
								<div class="col-3">
									<img
									class="d-block mx-auto"
									data-src="dist/img/satguys-b.png"
									alt="satguys Icon"
									width="75"
									>
								</div>

								<div class="col-9">
									<div class="row">
										<div class="col-5">
											<p class="title mb-0">
												<strong>satguys</strong>
											</p>
										</div>

										<div class="col-7 text-lg-right">
											<img
											class="trustpilot-logo"
											data-src="dist/img/trustpilot-logo.svg"
											alt="Trustpilot Logo"
											style="width: 120px; height:35px;"
											>
										</div>
									</div>

									<p class="mb-1">
										<strong>Reviews 1,220 <span class="d-inline-block mx-1">&bull;</span> Great</strong>
									</p>

									<div class="stars">
										<img
										data-src="dist/img/stars-4.svg" 
										alt="Trustpilot rating"
										style="width: 300px;"
										>
									</div>
								</div>
							</div>
						</div>

						<hr>

						<div class="title">
							<strong>"Natasha was great"</strong>
						</div>

						<p>
							"Natasha was great, really pleasant and helped me through the upgrade process. I rang customer services around 9.15am and only waited approx 3mins until somebody picked up."
						</p>

						<p>
							<strong>L.Dennison</strong>
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="comp__block py-3 blue">
			<div class="container text-center">
				<span class="body-number">
					<div class="row align-items-center">
						<div class="col-12 col-lg-6 text-lg-left">
							CALL: <a class="" href="tel:+441691662712">01691 662 712</a>
						</div>

						<div class="col-12 col-lg-6 text-lg-right">
							<small class="d-block">Lines open from 9am - 5pm, Mondays to Fridays</small>
						</div>
					</div>
				</span>
			</div>
		</section>

		<section class="comp__block white border-bottom" id="three-easy-steps">
			<div class="container">
				<h2 class="mb-4 text-lg-center">Satellite Broadband in 3 easy steps</h2>

				<div class="row">
					<div class="col-12 col-lg-4">
						<div class="row align-items-center mb-3" data-fadein>
							<div class="col-3 col-lg-12 mb-lg-3 text-center arrow-down">
								<img
								class="rounded-circle"
								data-src="dist/img/contact-us.svg"
								alt="Contact Us"
								>
							</div>

							<div class="col-9 col-lg-12 text-lg-center">
								<p class="mb-2">
									<strong>Get in touch</strong>
								</p>

								<p>
									Either give us a call or leave us your details for us to call you back to discuss your requirements. We can then advise on the most suitable package for your needs and outline all costs.
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-4">
						<div class="row align-items-center mb-3" data-fadein>
							<div class="col-3 col-lg-12 mb-lg-3 text-center arrow-down">
								<img
								class="rounded-circle"
								data-src="dist/img/schedule-payment.svg"
								alt="Schedule Payment"
								>
							</div>

							<div class="col-9 col-lg-12 text-lg-center">
								<p class="mb-2">
									<strong>Pay for installation and activation</strong>
								</p>

								<p>
									You will need to pay an installation and activation fee and set up your regular monthly payments.
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-4">
						<div class="row align-items-center" data-fadein>
							<div class="col-3 col-lg-12 mb-lg-3 text-center">
								<img
								class="rounded-circle"
								data-src="dist/img/connection.svg"
								alt="Connection"
								>
							</div>

							<div class="col-9 col-lg-12 text-lg-center">
								<p class="mb-2">
									<strong>Installation and connection</strong>
								</p>

								<p>
									We will put you in touch with one of our local installers, who will arrange a convenient time for installation. This typically takes less than 3 hours and you will be connected from then on.
								</p>
							</div>
						</div>
					</div>                
				</div>
			</div>
		</section>

		<section class="comp__block white text-center" data-fadein id="why-choose-us">
			<div class="container">
				<h2>Why Choose Us?</h2>

				<div class="row">
					<div class="col-12 col-lg-4">
						<div class="usp">
							<img
							class="mb-3"
							data-src="dist/img/local-installers-400x400.png" 
							alt="Local Installers"
							width="65"
							>

							<p class="text-uppercase mb-2">
								<strong>Local Installers</strong>
							</p>

							<p>
								Reliable network of engineers to help you get up and running.
							</p>
						</div>
					</div>

					<div class="col-12 col-lg-4">
						<div class="usp">
							<img
							class="mb-3"
							data-src="dist/img/coverage-400x400.png" 
							alt="Nationwide coverage"
							width="65"
							>

							<p class="text-uppercase mb-2">
								<strong>100% Nationwide coverage</strong>
							</p>

							<p>
								Contrary to popular belief, you can receive satellite broadband almost anywhere in the UK even without ADSL eligibility or a phone line.
							</p>
						</div>
					</div>

					<div class="col-12 col-lg-4">
						<div class="usp">
							<img
							class="mb-3"
							data-src="dist/img/peace-of-mind-400x400.png" 
							alt="Peace of mind"
							width="65"
							>

							<p class="text-uppercase mb-2">
								<strong>14 Day Cancellation</strong>
							</p>

							<p>
								If you're not completely satisfied simply cancel within 14 days of installation.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="comp__block grey" data-fadein id="trusted-by">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-xl-4">
						<h2 class="text-md-center text-xl-left mb-xl-0">Trusted by major corporations</h2>

						<img src="img/Microsoft.png" alt="Microsoft">
						<img src="img/Ford.png" alt="Ford">
						<img src="img/Google.png" alt="Google">

					</div>

					<div class="col-12 col-xl-8">
						<div class="text-center">
							<div class="d-block d-lg-inline-block">
								<img
								class="m-2"
								data-src="dist/img/8758acd7-cnn-logo_04y02e000000000000001.png"
								alt="CNN"
								width="70px"
								>

								<img
								class="m-2"
								data-src="dist/img/62599e42-bbc-logo_06g01w06e01u000000001.png"
								alt="BBC"
								width="95px"
								>
							</div>

							<img
							class="m-2"
							data-src="dist/img/c284250f-nhs-logo_054022052020000000001.png"
							alt="NHS"
							width="70px"
							>

							<img
							class="m-2"
							data-src="dist/img/32b58f2d-220px-rte-logo_04c02e04a02e000000001.png"
							alt="RTE"
							width="60px"
							>

							<img
							class="m-2"
							data-src="dist/img/bbaae9e7-tvp-logo_07w020000000000000001.png"
							alt="Thames Valley Police"
							width="120px"
							>

							<div class="d-block d-lg-inline-block">
								<img
								class="m-2"
								data-src="dist/img/6df5fc58-skynews-logo_07401k000000000000001.png"
								alt="Sky News"
								width="110px"
								>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="comp__block comp__review" data-fadein>
			<div class="container text-md-center text-xl-left">

				<div class="row">
					<div class="col-12 col-xl-4">
						<div class="stars">
							<img
							data-src="dist/img/stars-5.svg"
							alt="Trustpilot rating"
							width="250"
							>
						</div>
					</div>

					<div class="col-12 col-xl-8">
						<div class="title">
							<strong>"Monique was excellent"</strong>
						</div>

						<p>
							"Monique was excellent! Above and beyond service. Patient, knowledgeable thank you!"
						</p>
					</div>
				</div>


				<div class="row align-items-end">
					<div class="col-6 col-xl-8 order-xl-2">
						<strong>Lorraine</strong>
					</div>

					<div class="col-6 col-xl-4 order-xl-1">
						<img
						class="trustpilot-logo"
						data-src="dist/img/trustpilot-logo.svg"
						alt="Trustpilot Logo"
						>
					</div>
				</div>
			</div>
		</section>

		
<div id="accordion">
            <h3>Satellite Broadband</h3>
            <div>
            
                <button type="button" class="title mt-0 how-does">
                    How does satellite broadband work?
                </button>

                <div class="body">
                    <p>
                        As with any other internet service provider, we supply the link between your computer and the other computers in the world. The difference is that we use satellite technology to provide this link instead of phone lines or fibre-optic cables. 
                    </p>

                    <p>
                        With satellite broadband, you use a satellite dish mounted on your house to access the internet and don’t need to be connected to other infrastructure. Instead of going through underground cables, the information is beamed up to a satellite and then back to earth again.
                    </p>
                </div>
                
            </div>
            <h3>Does the weather affect my connection?</h3>
            <div>
                <p>
                    The honest answer is it can have a negative effect on the signal. But the weather has to be ‘extreme’ and the initial effect is to slow the system. It is important to note the overall KA Sat performance is 99.5%, which is better than almost all fixed-line services.
                </p>

            </div>
            <h3>What is the Upfront cost?</h3>
            <div>
                <p>
                    An activation fee of £49.99 is required along with an installation fee of £49.99. The installation fee is optional, you may install the equipment yourself if you choose to. Unlike other satellite broadband providers, we give you the option to rent your equipment from us so that you can spread the cost.
                </p>

                <p>
                    Monthly equipment rental is £2.50 for our bronze package and £5 for our silver and gold packages. The equipment is more expensive for silver and gold as it needs to handle the higher speeds offered by these packages.
                </p>

              
            </div>
          </div>


		<section class="comp__block comp__packages" id="packages" data-fadein>
			<div class="container">
				<h2 class="text-md-center mb-xl-5">Our Packages</h2>

				<div class="row">
					<div class="col-12 col-lg-4" data-fadein>
						<div class="package bronze">
							<div class="title">
								Konnect Bronze
							</div>

							<div class="price">
								£24.99<small>/month</small>
							</div>

							<ul class="summary-list">
								<li>
									<a href="#" class="helper-info average-speed">
										<span>
											<strong>12Mbps</strong> Average Speed
										</span>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											Mbps = Megabits per second.
										</p>

										<p>
											The speed will vary according to network traffic at any given time and typically during peak hours (5-11pm in the UK).
										</p>
									</span>
								</li>

								<li>
									<a href="#" class="helper-info data-usage">
										<strong>Unlimited Usage</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										Unlike other providers, we promise not to turn off your connection when you have reached your data allowance.
									</span>
								</li>

								<li>
									<a href="#" class="helper-info uptime">
										<strong>99.5% Uptime</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										This is the overall performance of our satellite, which is better than many fixed-line services.
									</span>
								</li>
							</ul>

							<p class="mb-1">
								<strong>Perfect for:</strong>
							</p>

							<ul>
								<li>
									Browsing the web
								</li>

								<li>
									Viewing emails
								</li>

								<li>
									Skype &amp; Facetime
								</li>
							</ul>

							<a
							href="#"
							target="_blank"
							class="btn btn-blue bronze-package"
							>
							Get Started
							</a>
						</div>
					</div>

					<div class="col-12 col-lg-4" data-fadein>
						<div class="package silver best-value scale-up">
							<div class="title">
								Konnect Silver
							</div>

							<div class="price">
								£39.99<small>/month</small>
							</div>

							<ul class="summary-list">
								<li>
									<a href="#" class="helper-info average-speed">
										<span>
											<strong>21Mbps</strong> Average Speed
										</span>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											Mbps = Megabits per second.
										</p>

										<p>
											The speed will vary according to network traffic at any given time and typically during peak hours (5-11pm in the UK).
										</p>
									</span>
								</li>

								<li>
									<a href="#" class="helper-info data-usage">
										<strong>Unlimited Usage</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											Unlike other providers, we promise not to turn off your connection when you have reached your data allowance.
										</p>
									</span>
								</li>

								<li>
									<a href="#" class="helper-info uptime">
										<strong>99.5% Uptime</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											This is the overall performance of our satellite, which is better than many fixed-line services.
										</p>
									</span>
								</li>
							</ul>

							<p class="mb-1">
								<strong>Perfect for:</strong>
							</p>

							<ul>
								<li>
									Multiple devices
								</li>

								<li>
									YouTube &amp; Netflix
								</li>

								<li>
									Working from home
								</li>
							</ul>

							<a
							href="#"
							target="_blank"
							class="btn btn-blue silver-package"
							>
							Get Started
							</a>
						</div>
					</div>

					<div class="col-12 col-lg-4" data-fadein>
						<div class="package gold">
							<div class="title">
								Konnect Gold
							</div>

							<div class="price">
								£69.99<small>/month</small>
							</div>

							<ul class="summary-list">
								<li>
									<a href="#" class="helper-info average-speed">
										<span>
											<strong>36Mbps</strong> Average Speed
										</span>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											Mbps = Megabits per second.
										</p>

										<p>
											The speed will vary according to network traffic at any given time and typically during peak hours (5-11pm in the UK).
										</p>
									</span>
								</li>

								<li>
									<a href="#" class="helper-info data-usage">
										<strong>Unlimited Usage</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											Unlike other providers, we promise not to turn off your connection when you have reached your data allowance.
										</p>
									</span>
								</li>

								<li>
									<a href="#" class="helper-info uptime">
										<strong>99.5% Uptime</strong>

										<span class="helper">
											<img class="open" src="dist/img/question-icon.svg">
											<img class="close" src="dist/img/times-icon.svg">
										</span>
									</a>

									<span class="helper-text">
										<p>
											This is the overall performance of our satellite, which is better than many fixed-line services.
										</p>
									</span>
								</li>
							</ul>

							<p class="mb-1">
								<strong>Perfect for:</strong>
							</p>

							<ul>
								<li>
									High quality streaming
								</li>

								<li>
									Multiple devices
								</li>

								<li>
									Large families
								</li>
							</ul>

							<a
							href="#"
							target="_blank"
							class="btn btn-blue gold-package"
							>
							Get Started
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="comp__block white hero-background">
			<div class="container text-center">
				<h2 class="blue-text">Got any questions?</h2>

				<a
				href="#"
				target="_blank"
				class="btn btn-orange mb-0 rounded-pill px-5 request-callback"
				>
				Get in touch
				</a>
			</div>
		</section>
	</main>

	<footer class="core__footer">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-xl-4">
					<p>
						<strong>
							<a class="footer-number" href="tel:+441691662712">01691 662 712</a>
						</strong>
					</p>

					<p>
						<small>The Old Smithy, Oswestry</small>
					</p>
				</div>

				<div class="col-12 col-xl-4">
					<p>
						<small>&copy;<?php echo date( 'Y' ); ?> satguys Broadband Plc.<br> All rights reserved.</small>
					</p>
				</div>

				<div class="col-12 col-xl-4">
					<img
					data-src="dist/img/ad0b8ec6-satguys-logo-white-rev-screen_0au05w0as04w00000i001.png"
					alt="satguys Logo Footer"
					width="200"
					>
				</div>
			</div>
		</div>
	</footer>

	<script src="dist/js/app.js" async defer></script>
</body>
</html>
