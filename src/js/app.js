require('../../node_modules/lite-youtube-embed/src/lite-yt-embed.js');

function throttle(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function() {
        previous = options.leading === false ? 0 : Date.now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
    };
    return function() {
        var now = Date.now();
        if (!previous && options.leading === false) previous = now;
        var remaining = wait - (now - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };
}

var navigationToggle = document.getElementById('nav-toggle');
var helperInfoLinks  = document.querySelectorAll('a.helper-info');
var accordionButtons = document.querySelectorAll('.comp__block .accordion');
var enquiryForm      = document.querySelector('.comp__form form');

if (helperInfoLinks) {
    helperInfoLinks.forEach(function(helperInfoLink) {
        helperInfoLink.addEventListener('click', function(e) {
            e.preventDefault();

            if(this.classList.contains('is-open')) {
                this.classList.remove('is-open');
            } else {
                this.classList.add('is-open');
            }
        });
    });
}

if (accordionButtons) {
    accordionButtons.forEach(function(accordionButton) {
        accordionButton.addEventListener('click', function(e) {
            e.preventDefault();

            var accordionBtn = accordionButton.querySelector('.title');
            var accordionBody = accordionButton.querySelector('.body');

            if (accordionBody.style.display === 'block') {
                accordionBody.style.display = 'none';
                accordionBtn.classList.remove('currently-selected');
            } else {
                accordionBody.style.display = 'block';
                accordionBtn.classList.add('currently-selected');
            }
        });
    });
}

if (navigationToggle) {
    navigationToggle.addEventListener('click', function(e) {
        e.preventDefault();
        
        var navigation = document.querySelector('.comp__navigation');

        if(this.classList.contains('is-open') && navigation.classList.contains('is-open')) {
            this.classList.remove('is-open');
            navigation.classList.remove('is-open');
        } else {
            this.classList.add('is-open');
            navigation.classList.add('is-open');
        }
    });
}

if (enquiryForm) {
    enquiryForm.addEventListener('submit', function() {
        var formBtn = enquiryForm.querySelector('button');
        formBtn.classList.add('form-submitted');
        formBtn.innerText = formBtn.dataset.sendingText;
    });

    enquiryForm.querySelector('#telephone-number').addEventListener('blur', function(e) {
    	var telValue = e.target.value;
    	e.target.value = telValue.split(' ').join('');
	});
}

window.onload = function() {   
    var embedVideos = document.querySelectorAll('iframe[data-src]');
    embedVideos.forEach(function(embedVideo) {
        embedVideo.src = embedVideo.dataset.src;
    });

    var lazyImages     = document.querySelectorAll('img[data-src]');
    var fadeInSections = document.querySelectorAll('[data-fadein]');
    var inAdvanced     = 150;

    function lazyLoadElements() {
        lazyImages.forEach(function(image) {
            if (image.offsetTop < window.innerHeight + window.pageYOffset + inAdvanced) {
                image.src = image.dataset.src;
            }
        });

        fadeInSections.forEach(function(section) {
            if (section.offsetTop < window.innerHeight + window.pageYOffset + inAdvanced) {
                section.classList.add('fadeIn');
            }
        });
    }

    if (window.innerHeight > 800) {
        lazyLoadElements();
    }
    
    window.addEventListener('scroll', throttle(lazyLoadElements, 50));
    window.addEventListener('resize', throttle(lazyLoadElements, 50));
};